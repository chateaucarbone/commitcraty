# Commitcraty

Installation artistique sur les futurs en construction



## Lignes de temps
Matériel : Rouleau de papier pour traceur Lisse Blanc Office Depot Non pelliculé 90 g/m² 91,4 cm x 45 m

Layout : 
- début : 5 images (2m).
- fin : 3 images (1.36m)

Pages à ajouter :
- 1 page titre
- 5 pages reference
- 1 page de fin vide
- Total : 23 m = 7 pages x 7 lignes x 45.5cm

Marges:
- côtés : 2cm ou pas
- haut : 5cm

| Lignes 	| Thèmes          										| Pages 			| Dimensions						|
| :----- 	|:----------------------------------	| :---------:	|:---------------------:|
| 1				| Fin. Guerre nucléaire 							| 16 (5+8+3)	| 7,5m 									|
| 2				| 																		| 38 (5+30+3)	| 17,5m	(3.5+6 pli+5) 	|
| 2-1			| 																		| 27 (24+3)		| 12,5m	(3.7+5 pli+3.3)	|
| 2-2			| 																		| 11 (8+3) 		| 5,5m 									|
| 3				| 																		| 30 (5+22+3)	| 14m										|
| 4				| 																		| 40 (5+32+3)	| 18m		(2+6 pli+6.5)		|
| 4-1			| 																		| 13 (13)			| 6m 										|
| 5				| 																		| 30 (5+22+3)	| 14m										|
| 6				| Fin.																|	10 (5+5)		| 4.6m									|
| 7				| 																		| 30 (5+22+3)	| 14m										|
| Total		|																			|							|	113.6m + 23m (sur 180m)		|


## Débit rouleaux

| Rouleau | Lignes     	  | Pages 				| Reste	|
| :----- 	|:------------  | :---------:		|:-----:|
| 1				| 2, 3				  | 82 (45+37)		| 16p |
| 2				| 1, 2-2, 7			| 84 (23+29+32)	| 18p	|
| 3				| 5, 6, 0			  | 81 (37+12+32)	| 17p |
| 4				| 2-1, 4, 4-1		| 76 (16+47+13)	| 22p |



## SVG

**Optimisation svg**

```scour -i input.svg -o output.svg --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none```


## PROCESSING


**Export pdf**

```
import processing.pdf.*;
 
PGraphics pdf = createGraphics(300, 300, PDF, "/home/user/Desktop/output.pdf");
pdf.beginDraw();
pdf.background(128, 0, 0);
pdf.line(50, 50, 250, 250);
((PGraphicsPDF) pdf).nextPage();
pdf.background(0, 128, 0);
pdf.line(50, 250, 250, 50);
pdf.dispose();
pdf.endDraw();
```