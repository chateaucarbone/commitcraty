
 /*
  * Export svg to pdf : deconstruction
  */

import geomerative.*;
import processing.pdf.*;

// SETTINGS
boolean export = true;
boolean ignoringStyles = true;
String title="ligne-4-1";
String effect = "split-contours";
int nb_pages = 2;
int nb_ref = 12;

int nb = nb_pages + 2 + nb_ref; // + title 1st page + blank last page + 5 pages refernce
String outfile = "../../pdf/"+title+"-"+nb+"p-"+effect+".pdf";
String infile = "../../svg/annee-reference.svg";
int page = 0;
RShape shp;
PGraphicsPDF pdf;

void setup(){

  // mm2px : ceil((455mm / 25.4 * 72ppi) = 1290 pixels
  size(1290, 1290); 
 
  //smooth();
  RG.init(this);
  shp = RG.loadShape(infile);
  shp = RG.centerIn(shp, g);
  RG.ignoreStyles(ignoringStyles);

  frameRate(5); // low rate for pdf export
  background(255);

  if (export) {
    pdf = (PGraphicsPDF)beginRecord(PDF, outfile);
    
    // First page title
    background(255);
    fill(0);
    textSize(100);
    text(title,width/2,height/2);
    noFill();
    pdf.nextPage();
  }
  

}

void draw(){
  // Transformation
  float t;
  
    // Colors
  background(255);
  
  // Transformation after reference pages
  if(page > nb_ref) {
    t = map(page - nb_ref, 0, nb_pages, 0.7, 0.6);
    if (ignoringStyles) {
      noFill();
      strokeWeight(map(page - nb_ref, 0, nb_pages, 1, 1.5));
      stroke(0);
    }
  } else {
    t = 0.7;  
  }
  
  // Shape
  translate(width/2, height/2);
  //rotate(PI/t*3);
  
  RShape[] splittedGroups = RG.split(shp, t); // t = interactif
  splittedGroups[0].draw();
  
  println("Page: " + page);
  println("Transformation: " + t);
  
  // Make pages
  if (page < nb_pages + nb_ref) {
     page++;
     if (export) {
       pdf.nextPage();
     }
  } else {    
     if (export) {
       endRecord();   
    }
    exit();
  }
 
}
