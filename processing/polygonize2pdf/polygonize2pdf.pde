
 /*
  * Export svg to pdf : Polygonize
  */

import geomerative.*;
import processing.pdf.*;


// SETTINGS
boolean export = true;

boolean ignoringStyles = false;
String title="ligne-2-2";
String effect = "polygonize";
int nb_pages = 11;
int nb_ref = 0;

int nb = nb_pages + 2 + nb_ref; // + title 1st page + blank last page + 5 pages refernce
String outfile = "../../pdf/"+title+"-"+nb+"p-"+effect+".pdf";
String infile = "../../svg/annee-reference.svg";
int page = 0;
RShape shp;
RShape polyshp;
PGraphicsPDF pdf;

void setup(){

  // mm2px : ceil((455mm / 25.4 * 72ppi) = 1290 pixels
  size(1290, 1290); 
 
  //smooth();
  RG.init(this);
  shp = RG.loadShape(infile);
  shp = RG.centerIn(shp, g, 100);
  RG.ignoreStyles(ignoringStyles);

  frameRate(5); // low rate for pdf export

  if (export) {
    pdf = (PGraphicsPDF)beginRecord(PDF, outfile);
    
    // First page title
    background(255);
    fill(0);
    textSize(100);
    text(title,width/2,height/2);
    pdf.nextPage();
  }

}

void draw(){
  // Transformation
  float t;
  
  // Transformation after reference pages
  if(page > nb_ref) {
    t = map(page - nb_ref, 0, nb_pages, 40, 50);
  } else {
    t = 0;  
  }
  
  // Colors
  background(255);
  if (ignoringStyles) {
    float gray = map(page - nb_ref, 0, nb_pages, 0, 255);
    fill(gray);
    stroke(gray);
  }
  
  // Polygonized
  RG.setPolygonizer(RG.UNIFORMLENGTH);
  RG.setPolygonizerLength(t);
  polyshp = RG.polygonize(shp);
  translate(width/2, height/2);
  scale(1.2);
  RG.shape(polyshp);
  
  println("Page: " + page);
  println("Transformation: " + t);
  
  // Make pages
  if (page < nb_pages + nb_ref) {
     page++;
     if (export) {
       pdf.nextPage();
     }
  } else {    
     if (export) {
       endRecord();   
    }
    exit();
  }
 
}
