import processing.pdf.*;
// from https://forum.processing.org/one/topic/change-dpi-of-the-size.html

int ppi = 72; // pixels per inch
int width_mm = 455; // width in millimeters
int height_mm = 455; // height in millimeters

PGraphics pdf;
int w,h;
  
void setup(){
  w = mm2px(width_mm, ppi);
  h = mm2px(height_mm, ppi);
  pdf = createGraphics(w, h, PDF, "/home/jr/Bureau/output.pdf");
  println(width_mm / 25.4 * 72);
  println(width_mm + " x " + height_mm + " mm");
  println(w + " x " + h + " px");
}


void draw(){
  /*
  pdf.beginDraw();
  pdf.background(128, 0, 0);
  pdf.line(50, 50, 250, 250);
  ((PGraphicsPDF) pdf).nextPage();
  pdf.background(0, 128, 0);
  pdf.line(50, 250, 250, 50);
  pdf.dispose();
  pdf.endDraw();
  */
  exit();
}

// convert millimeters to pixels, using the specified pixel density
int mm2px(float mm, float ppi) {
  return ceil((mm / 25.4 * ppi));
}
