
 /*
  * Export svg to pdf : Fade, deconstruction
  */

import geomerative.*;
import processing.pdf.*;


// SETTINGS
boolean export = true;

// Ligne 3
/*
boolean ignoringStyles = true;
String title="ligne-3";
String effect = "fade";
int nb_pages = 30;
int nb_ref = 5;
*/

// Ligne 1
boolean ignoringStyles = false;
String title="ligne-1";
String effect = "deconstruction";
int nb_pages = 16;
int nb_ref = 5;


int nb = nb_pages + 2 + nb_ref; // + title 1st page + blank last page + 5 pages refernce
String outfile = "../../pdf/"+title+"-"+nb+"p-"+effect+".pdf";
String infile = "../../svg/annee-reference.svg";
int page = 0;
RShape shp;
PGraphicsPDF pdf;

void setup(){

  // mm2px : ceil((455mm / 25.4 * 72ppi) = 1290 pixels
  size(1290, 1290); 
 
  //smooth();
  RG.init(this);
  shp = RG.loadShape(infile);
  shp = RG.centerIn(shp, g);
  RG.ignoreStyles(ignoringStyles);

  frameRate(1); // low rate for pdf export

  if (export) {
    pdf = (PGraphicsPDF)beginRecord(PDF, outfile);
    
    // First page title
    background(255);
    fill(0);
    textSize(100);
    text(title,width/2,height/2);
    pdf.nextPage();
  }

}

void draw(){
  // Transformation
  float t;
  
  // Transformation after reference pages
  if(page > nb_ref) {
    // t = map(page, 0, nb_pages + nb_ref, 0, 1); // Ligne 3
    t = map(page - nb_ref, 0, nb_pages, 1, 0); // Ligne 1
  } else {
    t = 1;  
  }
  
  // Colors
  background(255);
  if (ignoringStyles) {
    fill(t*255);
    stroke(t*255);
  }
  
  // Shape
  translate(width/2, height/2);
  
  // RShape[] splittedGroups = RG.split(shp, 1); // Ligne 3 : 1 = image total 
  RShape[] splittedGroups = RG.split(shp, t); // Ligne 1 : t = interactif
  
  splittedGroups[0].draw();
  
  println("Page: " + page);
  println("Transformation: " + t);
  
  // Make pages
  if (page < nb_pages + nb_ref) {
     page++;
     if (export) {
       pdf.nextPage();
     }
  } else {    
     if (export) {
       endRecord();   
    }
    exit();
  }
 
}
